# Jet Vertex Tagger - Track Based DNN

This is a tool for separating hard-scattering and pile-up jet.


# Download the package

Fork  [JVT_TBDNN](https://gitlab.cern.ch/jxiang/jvt_tbdnn_gittest)  on GitLab

Fork  [DYNET](https://github.com/clab/dynet)  on GitHub

Then run

```
git clone ssh://git@gitlab.cern.ch:7999/[username]/jvt_tbdnn_gittest.git
```

This project use a package [DYNET](https://github.com/clab/dynet) to process the neural network calculation. 

In the Project JVT_TBDNN, the compiled library of dynet is used. It is located in "Check_ROC/DNN_Track/Dynet_ana/lib/" 

The libdynet.so in that folder is complied under glibc 2.17 (Same as lxplus7). 

If you want to use this package in a system with different glibc version, please download the DYNET and complie it following the [instruction](https://github.com/clab/dynet/blob/master/README.md).

Then replace the libdynet.so with the newly complied one.


## Retrieve data from DAOD file

Login lxplus6

Go to JVT_TBDNN_GitTest/Publish_Jet_Track_Analysis/ folder

Then run

```
setupATLAS
mkdir build
mkdir run
cd build 
asetup 21.2.87,AnalysisBase
source */setup.sh
cmake ../source/
make
cd ..
voms-proxy-init -voms atlas  (Password is required)
lsetup rucio
lsetup panda
source ./source/RunGrid
```
After the Grid finish all jobs, an email will be sent to your CERN mailbox. Follow the instruction to download the file from user.[username].test.361022.e3668_s3126_r9364_p3404_hist/


## Train the TBDNN Model

Login Lxplus7

Go to jvt_tbdnn_gittest/Check_ROC/DNN_Track/Dynet_ana folder.

Modify the **JVT_TBDNN_Train.cxx** on Line #400 and Line #402, change the file path to the root file just downloaded by rucio.

Then run

```
root -l
gSystem->Load("lib/libdynet.so")
.x JVT_TBDNN_Train.cxx
```

The Model will be saved in ./model



## Test the Model

Login Lxplus7

Modify the **JVT_TBDNN_Test.cxx** on Line #405 and Line #407, change the file path to the root file just downloaded by rucio.

Modify the **JVT_TBDNN_Test.cxx** on Line #636, change the file path to the latest model in ./model folder.

Then run

```
root -l
gSystem->Load("lib/libdynet.so")
.x JVT_TBDNN_Test.cxx
```

A .json file and a .root file containing Test result will be created.

## Draw ROC curve and Calculate AUC score.

Open jvt_tbdnn_gittest/Check_ROC/TS_Draw_ROC/build/ folder

Open index.html with any browser.

Sample json files are in Check_ROC/json/


<div align="center">
  <img alt="DyNet" src="Pic/Calculator.png"><br><br>
</div>


