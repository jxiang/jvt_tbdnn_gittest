#include <srl_json_format.hpp>
#include <fstream>
#include <dy.hpp>

using namespace std;
using namespace tg;


class XorModel {
public:
    XorModel(dynet::ParameterCollection &pc) : linear0(pc, 2, 2), linear1(pc, 2, 1) {

    }

    bool predict(bool in0, bool in1) {
        auto input = dy::const_expr({in0 ? 1 : 0, in1 ? 1 : 0});
        auto x = dynet::logistic(linear0.forward(input));
        return dynet::as_scalar(dy::cg().forward(linear1.forward(x))) > 0;
    }

    dynet::Expression compute_loss(bool in0, bool in1, bool oracle) {
        auto input = dy::const_expr({in0 ? 1 : 0, in1 ? 1 : 0});
        auto x = dynet::logistic(linear0.forward(input));
        auto evidence = linear1.forward(x);
        auto confidence = dynet::logistic(evidence);
        auto oracle_expr = dy::const_expr(oracle ? 1 : 0);
        return dynet::binary_log_loss(confidence, oracle_expr);
    }

private:
    dy::linear_layer linear0;
    dy::linear_layer linear1;
};


int main() {
    dy::initialize();
    dynet::ParameterCollection pc;
    dynet::SimpleSGDTrainer trainer(pc);

    typedef std::tuple<bool, bool, bool> Datum;
    vector <Datum> training_data = {
        make_tuple(false, false, true),
        make_tuple(false, true, false),
        make_tuple(true, false, false),
        make_tuple(true, true, true)
    };

    XorModel my_model(pc);

    for (int epoch = 0; epoch < 3000; epoch++) {
        float sum_loss = 0;
        for (const auto &datum:training_data) {
            const auto loss = my_model.compute_loss(get<0>(datum), get<1>(datum), get<2>(datum));
            sum_loss += dynet::as_scalar(dy::cg().forward(loss));
            dy::cg().backward(loss);
            trainer.update();
        }
        if (epoch % 100 == 0) {
            cout << "epoch:" << epoch << " loss:" << sum_loss << endl;
        }

    }

    cout << "testing" << endl;

    for (const auto &datum:training_data) {
        const auto predicted = my_model.predict(get<0>(datum), get<1>(datum));
        cout << get<0>(datum) << get<1>(datum) << " => " << predicted << endl;
    }
}
