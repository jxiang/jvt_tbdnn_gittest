//
// Created by YAN Yuchen on 4/25/2018.
//

#ifndef FRAME_ANALYSIS_DYNET_LINEARLAYER_HPP
#define FRAME_ANALYSIS_DYNET_LINEARLAYER_HPP

#include "../dynet/dynet.h"
#include "../dynet/expr.h"
#include "dy_common.hpp"

namespace tg {
    namespace dy {
        class linear_layer {
        public:
            linear_layer() = default;

            linear_layer(const linear_layer &) = default;

            linear_layer(linear_layer &&) = default;

            linear_layer &operator=(const linear_layer &) = default;

            linear_layer &operator=(linear_layer &&) = default;

            linear_layer(dynet::ParameterCollection &model, unsigned dim_in, unsigned dim_out)
                : W(model.add_parameters({dim_out, dim_in})),
                  b(model.add_parameters({dim_out})) {
            }

            dynet::Expression operator()(const dynet::Expression &x) const {
                return forward(x);
            }

            dynet::Expression forward(const dynet::Expression &x) const {
                return dy::expr(W) * x + dy::expr(b);
            }

            dynet::Expression forward_given_output_positions(const dynet::Expression &x,
                                                             const std::vector<unsigned> output_positions) const {
                auto selected_W = dynet::select_rows(dy::expr(W), output_positions);
                auto selected_b = dynet::reshape(dynet::pick(dy::expr(b), output_positions),
                                                 {(unsigned) output_positions.size()});
                return selected_W * x + selected_b;
            }

        private:
            dynet::Parameter W;
            dynet::Parameter b;
        };
    }
}


#endif //FRAME_ANALYSIS_DYNET_LINEARLAYER_HPP
