//
// Created by YAN Yuchen on 5/1/2018.
//

#ifndef DYNET_WRAPPER_DY_LSTM_HPP
#define DYNET_WRAPPER_DY_LSTM_HPP

#include "../dynet/lstm.h"
#include "dy_common.hpp"
#include <memory>

namespace tg {
    namespace dy {
        /**
         * pure functional RNN adapter that wraps dynet::RNNBuilder
         * \tparam RNN_BUILDER_TYPE inherits dynet::RNNBuilder
         */
        template<class RNN_BUILDER_TYPE>
        class rnn {
        public:
            rnn() = default;

            rnn(const rnn &) = default;

            rnn(rnn &&) = default;

            rnn &operator=(const rnn &) = default;

            rnn &operator=(rnn &&) = default;

            /**
             * cell state is the thing that gets passed between time steps.
             * it's an concatenation of all layer's hidden states followed by all layer's cell states
             * normally you shouldn't be using it in any other way.
             */
            typedef std::vector<dynet::Expression> cell_state_type;

            explicit rnn(RNN_BUILDER_TYPE &&builder, float dropout_rate = 0) : builder(
                std::make_shared<RNN_BUILDER_TYPE>(builder)) {
                if (dropout_rate > 0) this->builder->set_dropout(dropout_rate);
            }

            /**
             * apply the RNN cell for a single time step
             * \param prev_state the previous cell state
             * \param x the current input
             * \return 0) the output
             *         1) the cell state after this time step
             */
            std::pair<dynet::Expression, cell_state_type>
            operator()(const cell_state_type &prev_state, const dynet::Expression &x) const {
                AUTO_START_THIS_GRAPH(builder->new_graph(cg()));
                builder->start_new_sequence(prev_state);
                auto y = builder->add_input(x);
                return std::make_pair(y, builder->final_s());
            }

            /**
             * apply the RNN cell for multiple time steps
             * \param prev_state the previous cell state
             * \param x_sequence a list of inputs to apply, in chronological order
             * \return 0) the list of output in chronological order
             *         1) the cell state after the last time step
             */
            std::pair<std::vector<dynet::Expression>, cell_state_type>
            operator()(const cell_state_type &prev_state, const std::vector<dynet::Expression> &x_sequence) const {
                AUTO_START_THIS_GRAPH(builder->new_graph(cg()));
                builder->start_new_sequence(prev_state);
                std::vector<dynet::Expression> y_sequence;
                for (auto itr = x_sequence.begin(); itr != x_sequence.end(); ++itr) {
                    y_sequence.push_back(builder->add_input(*itr));
                }
                return std::make_pair(y_sequence, builder->final_s());
            }

            static cell_state_type default_initial_state() { return {}; }

        private:
            std::shared_ptr<RNN_BUILDER_TYPE> builder;
        };
    }
}
#endif //DYNET_WRAPPER_DY_LSTM_HPP
