//
// Created by Xiang Jianhuan on OCT/30/2018.
//

#ifndef FRAME_ANALYSIS_DYNET_LINEARLAYERS_HPP
#define FRAME_ANALYSIS_DYNET_LINEARLAYERS_HPP

#include "../dynet/dynet.h"
#include "../dynet/expr.h"
#include "dy_common.hpp"
#include "dy_linear_layer.hpp"

namespace tg {
    namespace dy {
        class linear_layers {
        public:
            linear_layers(dynet::ParameterCollection &model, unsigned dim_in, unsigned dim_out, unsigned dim_distrib)
                : LS_dis() {
                for (int in_dis = 0; in_dis < dim_distrib; in_dis++) {
                    dy::linear_layer inputlayer(model, dim_in, dim_out);
                    LS_dis.push_back(inputlayer);
                }

            }


            vector <dy::linear_layer> LS_dis;

        private:

        };
    }
}


#endif //FRAME_ANALYSIS_DYNET_LINEARLAYERS_HPP
