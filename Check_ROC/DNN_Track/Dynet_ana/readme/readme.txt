Hello, This is a tutorial for TBDNN.

This is not the final version of TBDNN. A more compact version will be published soon.

==========================================================================================================================================

Content:

A. How to train.

B. How to use the trained model.

C. Data used.

==========================================================================================================================================

A. How to train:

1. Open JVT_TBDNN_Train.cxx.

2. Modify the path of input root file. Line 400,402. (Not Needed if using the dataset /Root_Data/Root_Data_25000/hist-mc16_13TeV.root)

3. Save and close JVT_TBDNN_Train.cxx.

4. setupATLAS

5. root -l

6. gSystem->Load("lib/libdynet.so");

7. .x JVT_TBDNN_Train.cxx.

8. Wait until "Save success" appears on screen.

9. The model will be stored in /Dynet_ana/model.

==========================================================================================================================================

B. How to use the trained model.

1. Open JVT_TBDNN_Test.cxx.

2. Modify the path of input root file. Line 400,402. (Not Needed if using the dataset /Root_Data/All_Track/hist-mc16_13TeV.root).

3. Choose the Model used in testing. Line 631.

4. Save and close JVT_TBDNN_Test.cxx.

5. setupATLAS

6. root -l

7. gSystem->Load("lib/libdynet.so");

8. .x JVT_TBDNN_Test.cxx.

9. "Trained_result.root" and corresponding DNN.json are generated.

==========================================================================================================================================
 
C. Data used.

1. The Dataset should contain the reconstructed track information.

2. Currently, all training and testing are done on the following dataset:

mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv.DAOD_TOPQ1.e3668_s3126_r9364_p3404










