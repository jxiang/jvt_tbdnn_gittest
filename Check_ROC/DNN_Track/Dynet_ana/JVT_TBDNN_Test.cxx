#include "TClass.h"
#include "TROOT.h"
#include "TSystem.h"
//#include "gsys.h"
#include <fstream>
#include "include/dyana/dy.hpp"
#include "include/dynet/io.h"
#include "TROOT.h"
#include "TSystem.h"
#include <vector>
#include "include/dyana/dy_linear_layers.hpp"

#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TStopwatch.h"
#include <chrono>

using namespace std;
using namespace tg;
using namespace chrono;

//gSystem->Load("libdynet.so");
//gSystem->Load("./libdynet.so");
//Int_t test=gSystem->Load("lib/libdynet.so");
//Int_t test=gSystem->Load("/mnt/e/Ubuntu_Folder/Research/2019-04-10/Dynet_ana/lib/libdynet.so");

int Linear_Layers_0_Number = 3;
int Linear_Layers_1_Number = 10;

bool sortcol(const vector<float> &v1, const vector<float> &v2) {
    return v1[0] > v2[0];
}

void return_current_time_and_date() {
    auto now = system_clock::now();
    auto in_time_t = system_clock::to_time_t(now);
    //cout << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X") << endl;
    return;
}
float Get_AUC_Score(const vector<vector<float>> &ROC_Vector) {
    float AUC_Score = -1;
    int bin_num = ROC_Vector.size();
    float BR_sum = 0;

    for (auto point:ROC_Vector) {
        BR_sum += point[1];
        //cout<<"( "<<point[0]<<","<<point[1]<<" )"<<endl;
    }

    AUC_Score = BR_sum / (bin_num + 0.0000001);
    return AUC_Score;
}

tuple<vector<vector<float>>, int, float>

ROC_Points(const vector<vector<float>> &Check_ROC_S, const vector<vector<float>> &Check_ROC_B,
           const int &bin_number) {
    int ROC_bins = bin_number;
    if (bin_number == 0) {
        ROC_bins = 1000;
    }
    float AUC_score = -1;
    vector<vector<float>> ROC_Vector;
    int Sig_size = Check_ROC_S.size();
    int BK_size = Check_ROC_B.size();
    int cut_num = Sig_size / ROC_bins;
    for (int i = 0; i < bin_number; i++) {
        vector<float> ROC_Point;
        int BK_num = 0;
        float Sig_Effi = ((i + 0.0000001) / (bin_number + 0.0000001));
        float Current_JVT = Check_ROC_S[i * cut_num][0];
        for (int i = 0; ((Check_ROC_B[i][0] > Current_JVT) && (i < BK_size)); i++) {
            BK_num += 1;
        }
        float BK_Rej = 1 - ((BK_num + 0.0000001) / (BK_size + 0.0000001));


        //cout<<" ( "<<Sig_Effi<<" , "<<BK_Rej<<" ) "<<endl;

        ROC_Point.push_back(Sig_Effi);
        ROC_Point.push_back(BK_Rej);
        ROC_Vector.push_back(ROC_Point);
    }
    AUC_score = Get_AUC_Score(ROC_Vector);
    tuple<vector<vector<float >>, int, float> Result_tuple = make_tuple(ROC_Vector, ROC_bins, AUC_score);
    return Result_tuple;
}



tuple<vector<tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<
    float >>> >, vector<tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<
    float >>> >>
Split_Data(vector<tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<
    float >>> >
           Input_data,
           int Training_num
) {
    vector<tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<
        float >>> >
        training_data;
    vector<tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<
        float >>> >
        testing_data;

    random_shuffle(Input_data.begin(), Input_data.end());
    int data_num = 0;

    for (const auto &datum:Input_data) {

        if (data_num < Training_num) {
            training_data.push_back(datum);
        } else {
            testing_data.push_back(datum);
        }
        data_num++;


    }

    tuple<vector<tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<
        float >>> >, vector<tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<
        float >>> >> Splitted_data = make_tuple(training_data, testing_data);

    return Splitted_data;

}




class TBDNN {

//gSystem->Load("libdynet.so");

public:
    TBDNN(dynet::ParameterCollection &pc) : linear0(pc, 8, 10), linear1(pc, 10, Linear_Layers_1_Number),
                                               linear2(pc, 8, 10),
                                               linear3(pc, 10, Linear_Layers_1_Number), linear4(pc, Linear_Layers_1_Number, 1),
                                               LS_0(pc, 3, 5, Linear_Layers_0_Number),
                                               LS_1(pc, 5, Linear_Layers_1_Number, Linear_Layers_1_Number) {

    }

    dynet::Expression
    forward(const tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<float >>

    > data) {

        int Sig_or_BK = get<0>(data);
        vector<float> rescaled_jet_info = get<1>(data);
        vector<vector<float>> rescaled_Track_PV = get<2>(data);
        vector<vector<float>> rescaled_Track_OV = get<3>(data);
        vector<vector<float>> rescaled_Track_UV = get<4>(data);

        TLorentzVector Jet_Pt_Eta_Phi_M;
        Jet_Pt_Eta_Phi_M.SetPtEtaPhiM(rescaled_jet_info[1], rescaled_jet_info[2], rescaled_jet_info[3],
                                      rescaled_jet_info[0]);
        float Jet_Theta = Jet_Pt_Eta_Phi_M.Theta();
        float Jet_Phi = Jet_Pt_Eta_Phi_M.Phi();
        TVector3 Jet_Direction;
        Jet_Direction.SetMagThetaPhi(1, Jet_Theta, Jet_Phi);
        float Jet_Dir_X = Jet_Direction.x();
        float Jet_Dir_Y = Jet_Direction.y();
        float Jet_Dir_Z = Jet_Direction.z();

        vector<float> Processing_jet_info;
        Processing_jet_info.push_back(rescaled_jet_info[0]);
        Processing_jet_info.push_back(rescaled_jet_info[1]);
        Processing_jet_info.push_back(Jet_Dir_X);
        Processing_jet_info.push_back(Jet_Dir_Y);
        Processing_jet_info.push_back(Jet_Dir_Z);
        Processing_jet_info.push_back(rescaled_jet_info[4]);
        Processing_jet_info.push_back(rescaled_jet_info[5]);
        Processing_jet_info.push_back(rescaled_jet_info[6]);

        auto jet_info_input = dy::const_expr(Processing_jet_info);
        auto mid_select_jet = dynet::logistic(linear0.forward(jet_info_input));
        auto result_select_jet = dynet::logistic(linear1.forward(mid_select_jet));
        dynet::Expression sum_PV_select = result_select_jet;
        dynet::Expression sum_OV_select = result_select_jet;
        dynet::Expression sum_UV_select = result_select_jet;

        //PV

        for (auto track_PV : rescaled_Track_PV) {
            vector<float> Current_Track;
            Current_Track.push_back(track_PV[1]);
            Current_Track.push_back(track_PV[2]);
            Current_Track.push_back(track_PV[3]);

            auto track_info_input = dy::const_expr(Current_Track);
            auto mid_select_track = dynet::logistic(LS_0.LS_dis[0].forward(track_info_input));

            for (int i_matrix = 0; i_matrix < Linear_Layers_1_Number; i_matrix++) {
                auto mid_sep_select_track = dynet::logistic(LS_1.LS_dis[i_matrix].forward(mid_select_track));
                sum_PV_select = sum_PV_select + mid_sep_select_track;
            }

        }


        //OV

        for (auto track_OV : rescaled_Track_OV) {
            vector<float> Current_Track;
            Current_Track.push_back(track_OV[1]);
            Current_Track.push_back(track_OV[2]);
            Current_Track.push_back(track_OV[3]);

            auto track_info_input = dy::const_expr(Current_Track);
            auto mid_select_track = dynet::logistic(LS_0.LS_dis[1].forward(track_info_input));

            for (int i_matrix = 0; i_matrix < Linear_Layers_1_Number; i_matrix++) {
                auto mid_sep_select_track = dynet::logistic(LS_1.LS_dis[i_matrix].forward(mid_select_track));
                sum_OV_select = sum_OV_select + mid_sep_select_track;
            }

        }

        //UV

        for (auto track_UV : rescaled_Track_UV) {
            vector<float> Current_Track;
            Current_Track.push_back(track_UV[1]);
            Current_Track.push_back(track_UV[2]);
            Current_Track.push_back(track_UV[3]);

            auto track_info_input = dy::const_expr(Current_Track);
            auto mid_select_track = dynet::logistic(LS_0.LS_dis[2].forward(track_info_input));

            for (int i_matrix = 0; i_matrix < Linear_Layers_1_Number; i_matrix++) {
                auto mid_sep_select_track = dynet::logistic(LS_1.LS_dis[i_matrix].forward(mid_select_track));
                sum_UV_select = sum_UV_select + mid_sep_select_track;
            }

        }


        auto select_PV_coefficient = dynet::logistic(sum_PV_select);
        auto select_OV_coefficient = dynet::logistic(sum_OV_select);
        auto select_UV_coefficient = dynet::logistic(sum_UV_select);

        auto mid_calculation_jet = dynet::logistic(linear2.forward(jet_info_input));
        auto result_calculation_jet = dynet::logistic(linear3.forward(mid_calculation_jet));
        dynet::Expression jet_calculation = result_calculation_jet;

        //PV

        for (auto track_PV : rescaled_Track_PV) {
            vector<float> Current_Track;
            Current_Track.push_back(track_PV[1]);
            Current_Track.push_back(track_PV[2]);
            Current_Track.push_back(track_PV[3]);

            auto track_info_input = dy::const_expr(Current_Track);
            auto mid_calculation_track = dynet::logistic(LS_0.LS_dis[0].forward(track_info_input));

            for (int i_matrix = 0; i_matrix < Linear_Layers_1_Number; i_matrix++) {
                auto mid_sep_calculation_track = dynet::logistic(
                    (LS_1.LS_dis[i_matrix].forward(mid_calculation_track)) *
                    (dynet::pick(select_PV_coefficient, i_matrix)));
                jet_calculation = jet_calculation + mid_sep_calculation_track;
            }

        }

        //OV

        for (auto track_OV : rescaled_Track_OV) {
            vector<float> Current_Track;
            Current_Track.push_back(track_OV[1]);
            Current_Track.push_back(track_OV[2]);
            Current_Track.push_back(track_OV[3]);

            auto track_info_input = dy::const_expr(Current_Track);
            auto mid_calculation_track = dynet::logistic(LS_0.LS_dis[1].forward(track_info_input));

            for (int i_matrix = 0; i_matrix < Linear_Layers_1_Number; i_matrix++) {
                auto mid_sep_calculation_track = dynet::logistic(
                    (LS_1.LS_dis[i_matrix].forward(mid_calculation_track)) *
                    (dynet::pick(select_OV_coefficient, i_matrix)));
                jet_calculation = jet_calculation + mid_sep_calculation_track;
            }

        }

        //UV

        for (auto track_UV : rescaled_Track_UV) {
            vector<float> Current_Track;
            Current_Track.push_back(track_UV[1]);
            Current_Track.push_back(track_UV[2]);
            Current_Track.push_back(track_UV[3]);

            auto track_info_input = dy::const_expr(Current_Track);
            auto mid_calculation_track = dynet::logistic(LS_0.LS_dis[2].forward(track_info_input));

            for (int i_matrix = 0; i_matrix < Linear_Layers_1_Number; i_matrix++) {
                auto mid_sep_calculation_track = dynet::logistic(
                    (LS_1.LS_dis[i_matrix].forward(mid_calculation_track)) *
                    (dynet::pick(select_UV_coefficient, i_matrix)));
                jet_calculation = jet_calculation + mid_sep_calculation_track;
            }

        }

        auto jet_result_mid = dynet::logistic(jet_calculation);
        auto jvt_result = linear4.forward(jet_result_mid);
        return jvt_result;

    }

    float
    predict(const tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<float >>

    > data) {
        auto mid = forward(data);
        return dynet::as_scalar(dy::cg().forward(mid));
    }


    dynet::Expression compute_loss(const tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>,
        vector<vector<float >>

    > data) {

        auto mid = forward(data);
        auto confidence = dynet::logistic(mid);
        int oracle = get<0>(data);
        auto oracle_expr = dy::const_expr(oracle);
        auto Modified_Loss = dynet::binary_log_loss(confidence, oracle_expr);
        //return (Modified_Loss*dy::const_expr(1+500*(1-oracle)));
        return Modified_Loss;
    }

    /*

    string export_structure() {
      return string::to_string(_dim_in)
    }
    */
    /*
    public static TBDNN import_structure(dynet::ParameterCollection& pc, int changedim) {
      return new TBDNN(pc, changedim);

    }
    */


private:
    //unsigned _dim_in;
    dy::linear_layer linear0;
    dy::linear_layer linear1;
    dy::linear_layer linear2;
    dy::linear_layer linear3;
    dy::linear_layer linear4;
    dy::linear_layers LS_0;
    dy::linear_layers LS_1;
};







int JVT_TBDNN_Test() {
//gSystem->Load("libdynet.so");
    dy::initialize();
    dynet::ParameterCollection pc;
    dynet::SimpleSGDTrainer trainer(pc);


/*
  typedef std::tuple<bool, bool, bool> Datum;
  vector<Datum> training_data = {
    make_tuple(false,false,true),
    make_tuple(false,true,false),
    make_tuple(true,false,false),
    make_tuple(true,true,true)
  };
*/


    float t_jvfcorr, t_rpt, t_pt, t_eta;
    float Category_cat1, Category_cat2;
    float jetinfo[9];
    float jetevaluated[10];
    vector<float> *jettrackinfo = new vector<float>;


//    TChain *chain0 = new TChain("TreeS");
//    chain0->Add("Root_Data/All_Track/hist-mc16_13TeV.root");
//    TChain *chain1 = new TChain("TreeB");
//    chain1->Add("Root_Data/All_Track/hist-mc16_13TeV.root");

    TChain *chain0 = new TChain("TreeS");
    chain0->Add("/eos/user/j/jxiang/Grid_JVT_Data/user.jxiang.test.361022.e3668_s3126_r9364_p3404_hist/user.jxiang.18636943._000001.hist-output.root");
    TChain *chain1 = new TChain("TreeB");
    chain1->Add("/eos/user/j/jxiang/Grid_JVT_Data/user.jxiang.test.361022.e3668_s3126_r9364_p3404_hist/user.jxiang.18636943._000001.hist-output.root");

    chain0->SetBranchAddress("jetinfo", &jetinfo);
    chain1->SetBranchAddress("jetinfo", &jetinfo);
    chain0->SetBranchAddress("jettrackinfo", &jettrackinfo);
    chain1->SetBranchAddress("jettrackinfo", &jettrackinfo);

    //vector<vector<float>> training_data;
    vector<tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<
        float >>> >
        training_data;  // int(S=1/B=0)  ,vector<float> jet(p4,n_pv,n_ov,n_uv) ,vector<vector<float>> Tracks_PV,vector<vector<float>> Tracks_OV,,vector<vector<float>> Tracks_OV



    int nentries0 = (int) chain0->GetEntries();
    int nentries1 = (int) chain1->GetEntries();

    int Sig_num = 0;
    int BK_num = 0;
    int Max_Track = 0;
    int Vector_size = 30;
    float Max_m = 0;
    float Max_pt = 0;
    float Max_eta = 0;
    float Max_phi = 0;
    float Max_deta = 0;
    float Max_dphi = 0;
    int Max_num_SG_Track = 0;
    int Max_num_BK_Track = 0;

    //TH1F* h1 = new TH1F("h1", "h1 title", 100, 0.0, 500.0);


    for (int i = 25001; i < nentries0; i++) {
        chain0->GetEntry(i);


        t_jvfcorr = jetinfo[5];
        t_rpt = jetinfo[6];
        t_eta = jetinfo[3];
        t_pt = jetinfo[2];

        float jet_m = jetinfo[1];
        float jet_pt = jetinfo[2];
        float jet_eta = jetinfo[3];
        float jet_phi = jetinfo[4];

        float rs_jet_m = jet_m / 20000;
        float rs_jet_pt = jet_pt / 120000;
        float rs_jet_eta = jet_eta / 3;
        float rs_jet_phi = jet_phi / 3;

        //cout<<(*jettrackinfo)[0]<<endl;

        //Tracknum/20




        int Sig_or_BK = 1;
        vector<float> rescaled_jet_info;
        vector<vector<float>> rescaled_Track_PV;
        vector<vector<float>> rescaled_Track_OV;
        vector<vector<float>> rescaled_Track_UV;

        rescaled_jet_info.push_back(rs_jet_m);
        rescaled_jet_info.push_back(rs_jet_pt);
        rescaled_jet_info.push_back(rs_jet_eta);
        rescaled_jet_info.push_back(rs_jet_phi);


        int trackinfo_size = jettrackinfo->size();
        int Correct_Num = 0;
        if ((trackinfo_size % 5) != 2) {
            //Incorrect_Num=1;
            cout << "Error" << endl;

        }
        int track_count = (trackinfo_size / 5) - 2;

        for (int i_track = 0; i_track < track_count; i_track++) {
            //cout << jettrackinfo->at(0) << endl;
            //cout << (*jettrackinfo)[0] << endl;
            //cout << i<<"   |   "<<(*jettrackinfo)[i_track*5+6]<<"   |   "<<(*jettrackinfo)[i_track*5+3] << endl;

            //h1->Fill((*jettrackinfo)[i_track*5+2]);


            vector<float> Target_Track_info;
            float rs_T_m = (*jettrackinfo)[i_track * 5 + 2] / 20000;
            float rs_T_pt = (*jettrackinfo)[i_track * 5 + 3] / 120000;
            float rs_T_eta = (*jettrackinfo)[i_track * 5 + 4] / 3;
            float rs_T_phi = (*jettrackinfo)[i_track * 5 + 5] / 3;
            Target_Track_info.push_back(rs_T_m);
            Target_Track_info.push_back(rs_T_pt);
            Target_Track_info.push_back(rs_T_eta - rs_jet_eta);
            Target_Track_info.push_back(rs_T_phi - rs_jet_phi);
            if ((*jettrackinfo)[i_track * 5 + 6] == 1) {
                rescaled_Track_PV.push_back(Target_Track_info);
            } else if ((*jettrackinfo)[i_track * 5 + 6] == 2) {
                rescaled_Track_OV.push_back(Target_Track_info);
            } else if ((*jettrackinfo)[i_track * 5 + 6] == -1) {
                rescaled_Track_UV.push_back(Target_Track_info);
            } else {
                cout << "ERROR" << endl;
            }


        }
        //cout<<"( "<<rescaled_Track_PV.size()<<"|| "<<rescaled_Track_OV.size()<<"|| "<<rescaled_Track_UV.size()<<")"<<endl;
        rescaled_jet_info.push_back(rescaled_Track_PV.size());
        rescaled_jet_info.push_back(rescaled_Track_OV.size());
        rescaled_jet_info.push_back(rescaled_Track_UV.size());

        tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<
            float >>> rs_JET;
        rs_JET = make_tuple(Sig_or_BK, rescaled_jet_info, rescaled_Track_PV, rescaled_Track_OV, rescaled_Track_UV);
        training_data.push_back(rs_JET);


    }


    for (int i = 25001; i < nentries1; i++) {
        chain1->GetEntry(i);


        t_jvfcorr = jetinfo[5];
        t_rpt = jetinfo[6];
        t_eta = jetinfo[3];
        t_pt = jetinfo[2];

        float jet_m = jetinfo[1];
        float jet_pt = jetinfo[2];
        float jet_eta = jetinfo[3];
        float jet_phi = jetinfo[4];

        float rs_jet_m = jet_m / 20000;
        float rs_jet_pt = jet_pt / 120000;
        float rs_jet_eta = jet_eta / 3;
        float rs_jet_phi = jet_phi / 3;

        //cout<<(*jettrackinfo)[0]<<endl;

        //Tracknum/20




        int Sig_or_BK = 0;
        vector<float> rescaled_jet_info;
        vector<vector<float>> rescaled_Track_PV;
        vector<vector<float>> rescaled_Track_OV;
        vector<vector<float>> rescaled_Track_UV;

        rescaled_jet_info.push_back(rs_jet_m);
        rescaled_jet_info.push_back(rs_jet_pt);
        rescaled_jet_info.push_back(rs_jet_eta);
        rescaled_jet_info.push_back(rs_jet_phi);


        int trackinfo_size = jettrackinfo->size();
        int Correct_Num = 0;
        if ((trackinfo_size % 5) != 2) {
            //Incorrect_Num=1;
            cout << "Error" << endl;

        }
        int track_count = (trackinfo_size / 5) - 2;

        for (int i_track = 0; i_track < track_count; i_track++) {
            //cout << jettrackinfo->at(0) << endl;
            //cout << (*jettrackinfo)[0] << endl;
            //cout << i<<"   |   "<<(*jettrackinfo)[i_track*5+6]<<"   |   "<<(*jettrackinfo)[i_track*5+3] << endl;

            //h1->Fill((*jettrackinfo)[i_track*5+2]);


            vector<float> Target_Track_info;
            float rs_T_m = (*jettrackinfo)[i_track * 5 + 2] / 20000;
            float rs_T_pt = (*jettrackinfo)[i_track * 5 + 3] / 120000;
            float rs_T_eta = (*jettrackinfo)[i_track * 5 + 4] / 3;
            float rs_T_phi = (*jettrackinfo)[i_track * 5 + 5] / 3;
            Target_Track_info.push_back(rs_T_m);
            Target_Track_info.push_back(rs_T_pt);
            Target_Track_info.push_back(rs_T_eta - rs_jet_eta);
            Target_Track_info.push_back(rs_T_phi - rs_jet_phi);
            if ((*jettrackinfo)[i_track * 5 + 6] == 1) {
                rescaled_Track_PV.push_back(Target_Track_info);
            } else if ((*jettrackinfo)[i_track * 5 + 6] == 2) {
                rescaled_Track_OV.push_back(Target_Track_info);
            } else if ((*jettrackinfo)[i_track * 5 + 6] == -1) {
                rescaled_Track_UV.push_back(Target_Track_info);
            } else {
                cout << "ERROR" << endl;
            }


        }
        //cout<<"( "<<rescaled_Track_PV.size()<<"|| "<<rescaled_Track_OV.size()<<"|| "<<rescaled_Track_UV.size()<<")"<<endl;
        rescaled_jet_info.push_back(rescaled_Track_PV.size());
        rescaled_jet_info.push_back(rescaled_Track_OV.size());
        rescaled_jet_info.push_back(rescaled_Track_UV.size());

        tuple<int, vector<float>, vector<vector<float >>, vector<vector<float >>, vector<vector<
            float >>> rs_JET;
        rs_JET = make_tuple(Sig_or_BK, rescaled_jet_info, rescaled_Track_PV, rescaled_Track_OV, rescaled_Track_UV);
        training_data.push_back(rs_JET);


    }

    /*
    TCanvas *c1 = new TCanvas("c1","c1",800,1000);
    c1->cd(1);
    h1->Draw();
     */






    cout << "Data collected" << endl;
    cout << "Data Size: " << training_data.size() << endl;
    random_shuffle(training_data.begin(), training_data.end());


    TBDNN my_model(pc);
    dynet::TextFileLoader l("./model/DNN_model_91931_80000_49.model");
    l.populate(pc);
    cout << "Model Imported" << endl;



    //dynet::TextFileSaver s("./DNN.model");
    //s.save(pc);


    TFile *fN = new TFile("Trained_result.root", "RECREATE");
    TTree *s_tree = new TTree("Evaluated_TreeS", "Evaluated_TreeS");
    s_tree->SetDirectory(fN);
    TTree *b_tree = new TTree("Evaluated_TreeB", "Evaluated_TreeB");
    b_tree->SetDirectory(fN);
    s_tree->Branch("jetevaluated", &jetevaluated, "jetevaluated[5]/F");
    b_tree->Branch("jetevaluated", &jetevaluated, "jetevaluated[5]/F");

    vector<vector<vector<float>>>Output_Root_Result;
    vector<vector<float>>Signal_data;
    vector<vector<float>>Background_data;

    int counting_num=0;

    for (const auto &datum:training_data) {
        const auto predicted = my_model.predict(datum);

        int Sig_or_BK = get<0>(datum);
        vector<float> jetevaluated_now;
        vector<float> rescaled_jet_info = get<1>(datum);

        vector<float>Data_jet_input;

        jetevaluated[0] = 0;//jvfcorr
        jetevaluated[1] = 0;//RpT_
        jetevaluated[2] = rescaled_jet_info[2] * 3;//eta_
        jetevaluated[3] = rescaled_jet_info[1] * 120000;//pt_
        jetevaluated[4] = predicted;

        Data_jet_input.push_back(jetevaluated[0]);
        Data_jet_input.push_back(jetevaluated[1]);
        Data_jet_input.push_back(jetevaluated[2]);
        Data_jet_input.push_back(jetevaluated[3]);
        Data_jet_input.push_back(jetevaluated[4]);

        if(counting_num%10000==0){
            cout<<counting_num<<endl;
        }
        counting_num++;



        if (Sig_or_BK == 1) {
            Signal_data.push_back(Data_jet_input);
            s_tree->Fill();
        } else if (Sig_or_BK == 0) {
            Background_data.push_back(Data_jet_input);
            b_tree->Fill();
        } else {
            cout << "Error" << endl;
        }
        dy::renew_cg();
    }

    Output_Root_Result.push_back(Signal_data);
    Output_Root_Result.push_back(Background_data);

    auto json = TBufferJSON::ToJSON(&Output_Root_Result);
    ofstream fout("DNN.json");
    fout<<json;
    fout.close();


    cout << "success" << endl;
    fN->Write();
    cout << "Save success" << endl;














    //End
    return_current_time_and_date();



    delete chain0;

    delete chain1;

    delete jettrackinfo;

    delete fN;



    return 0;
}


/*
 *
 *
 *
 *
 */







