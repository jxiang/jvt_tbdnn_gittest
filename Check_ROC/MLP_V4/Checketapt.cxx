
#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
using namespace TMVA;
void Checketapt()
{
     // This loads the library
     TMVA::Tools::Instance();
     // Create the Reader object
     TMVA::Reader *reader = new TMVA::Reader( "!Color:!Silent" );
     Float_t jvfcorr, RpT_, eta_, pt_;
     Float_t Category_cat1, Category_cat2;
     Float_t jetinfo[9];





     Float_t jetevaluated[5];
     
     TChain *chain0=new TChain("TreeS");
     chain0->Add("../../Root_Data/All_Track/hist-mc16_13TeV.root");
     TChain *chain1=new TChain("TreeB");
     chain1->Add("../../Root_Data/All_Track/hist-mc16_13TeV.root");
     
     TFile *fN = new TFile("Checketapt.root","RECREATE");

     TTree *s_tree;
     TTree *b_tree;
     
     s_tree = new TTree ("Evaluated_TreeS", "Evaluated_TreeS");
     s_tree->SetDirectory (fN);  
  
     b_tree = new TTree ("Evaluated_TreeB", "Evaluated_TreeB");
     b_tree->SetDirectory (fN);  
     
     
     
     
     s_tree->Branch ("jetevaluated", &jetevaluated, "jetevaluated[5]/F");
     b_tree->Branch ("jetevaluated", &jetevaluated, "jetevaluated[5]/F");
     
     chain0->SetBranchAddress("jetinfo",&jetinfo);
     chain1->SetBranchAddress("jetinfo",&jetinfo);
     
     ////TMVA
     
     //TMVA::Reader *reader = new TMVA::Reader("!Color");
    reader->AddVariable( "jvfcorr", &jvfcorr );
    reader->AddVariable( "RpT_", &RpT_ );
    reader->AddVariable( "eta_", &eta_ );
    reader->AddVariable( "pt_", &pt_ );
    reader->BookMVA( "MLPBNN", "./dataset/weights/TMVAClassification_MLPBNN.weights.xml" );


    vector<vector<vector<float>>>Output_Root_Result;
    vector<vector<float>>Signal_data;
    vector<vector<float>>Background_data;
     
     
     
     
     
     
     
     
     
     Int_t nentries0=(Int_t)chain0->GetEntries();


	 for(Int_t i=0;i<nentries0 ;i++)
	 
       {
              chain0->GetEntry(i);

           vector<float>Sig_input;
              
              
              
              jetevaluated[0]=jetinfo[5];//jvfcorr
              jetevaluated[1]=jetinfo[6];//RpT_
              jetevaluated[2]=jetinfo[3];//eta_
              jetevaluated[3]=jetinfo[2];//pt_

           jvfcorr = jetinfo[5];
           RpT_ = jetinfo[6];
           eta_ = jetinfo[3];
           pt_ = jetinfo[2];
              float JVT = reader->EvaluateMVA( "MLPBNN" );
              jetevaluated[4] = JVT;

           Sig_input.push_back(jetevaluated[0]);
           Sig_input.push_back(jetevaluated[1]);
           Sig_input.push_back(jetevaluated[2]);
           Sig_input.push_back(jetevaluated[3]);
           Sig_input.push_back(jetevaluated[4]);

           Signal_data.push_back(Sig_input);
         
              s_tree->Fill();
              
    
       }
       
       
       
       
       
       
     Int_t nentries1=(Int_t)chain1->GetEntries();


	 for(Int_t i=0;i<nentries1 ;i++)
	 
       {
              chain1->GetEntry(i);

           vector<float>BK_input;



           jetevaluated[0]=jetinfo[5];//corrjvf
           jetevaluated[1]=jetinfo[6];//RpT_
           jetevaluated[2]=jetinfo[3];//eta_
           jetevaluated[3]=jetinfo[2];//pt_

           jvfcorr = jetinfo[5];
           RpT_ = jetinfo[6];
           eta_ = jetinfo[3];
           pt_ = jetinfo[2];
           float JVT = reader->EvaluateMVA( "MLPBNN" );
           jetevaluated[4] = JVT;

           BK_input.push_back(jetevaluated[0]);
           BK_input.push_back(jetevaluated[1]);
           BK_input.push_back(jetevaluated[2]);
           BK_input.push_back(jetevaluated[3]);
           BK_input.push_back(jetevaluated[4]);
           Background_data.push_back(BK_input);
         
              b_tree->Fill();
              
    
       }



    Output_Root_Result.push_back(Signal_data);
    Output_Root_Result.push_back(Background_data);

    auto json = TBufferJSON::ToJSON(&Output_Root_Result);
    ofstream fout("MLP_V4.json");
    fout<<json;
    fout.close();
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       cout<<"success"<<endl;
       fN->Write();
     

     
     
     return;
     
}
